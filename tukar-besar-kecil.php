<?php
function tukar_besar_kecil($string){
    $hasil = "";
    echo "Dari " . $string . " menjadi: ";
    for($i=0; $i<strlen($string);$i++){
        $huruf = substr($string, $i, 1);

        if($huruf == " "){
            echo " ";
        }
        elseif(ctype_alpha($huruf)){
            if(ctype_upper($huruf)){
                $hasil = strtolower($huruf);
                echo $hasil;
            }
            elseif(ctype_lower($huruf)){
                $hasil = strtoupper($huruf);
                echo $hasil;
            }
        }
        else{
            echo $huruf;
        }
        }  
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>