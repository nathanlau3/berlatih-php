<?php
function ubah_huruf($string){
    $hasil = "";
    echo "Dari " . $string . " menjadi: ";
    for($i=0; $i<strlen($string);$i++){
        $huruf = substr($string, $i, 1);
        $hasil = ++$huruf;
        echo $hasil;
        }  
}

// TEST CASES
echo ubah_huruf("wow"); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>